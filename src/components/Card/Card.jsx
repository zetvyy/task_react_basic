import React from "react";
import "./card.css";

function Card(props) {
  const { name, alamat } = props;
  return (
    <>
      <h1 id="header">Halo My Name is {name}</h1>
      <p id="paragraf">I'm From {alamat}</p>
    </>
  );
}

export default Card;
