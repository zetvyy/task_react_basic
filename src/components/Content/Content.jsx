import React from "react";
import Card from "./../Card/Card";
import "./content.css";

class Content extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "Zaidan",
      alamat: "Malang, East Java"
    };
  }
  render() {
    return (
      <div className="content">
        <Card name={this.state.name} alamat={this.state.alamat} />
        <div className="btn">
          <button onClick={() => this.setState({ name: "Messi" })}>Ubah Nama</button>
          <button onClick={() => this.setState({ alamat: "Barcelona, Spain" })}>Ubah Alamat</button>
        </div>
      </div>
    );
  }
}

export default Content;
